#[repr(u8)]
#[derive(Debug, PartialEq, Clone)]
pub enum Seat {
    Occupied,
    Empty,
    Floor,
}

impl std::fmt::Display for Seat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Occupied => write!(f, "#"),
            Self::Empty => write!(f, "L"),
            Self::Floor => write!(f, "."),
        }
    }
}
impl From<&u8> for Seat {
    fn from(input: &u8) -> Self {
        match input {
            &b'#' => Self::Occupied,
            &b'L' => Self::Empty,
            &b'.' => Self::Floor,
            _ => unimplemented!(),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Row(Vec<Seat>);

impl std::fmt::Display for Row {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for seat in &self.0 {
            write!(f, "{}", seat).unwrap();
        }

        write!(f, "\n")
    }
}

impl From<Vec<Seat>> for Row {
    fn from(row: Vec<Seat>) -> Self {
        Row(row)
    }
}

impl Into<Vec<Seat>> for Row {
    fn into(self) -> Vec<Seat> {
        self.0
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Area(Vec<Row>);

impl std::fmt::Display for Area {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in &self.0 {
            write!(f, "{}", row).unwrap();
        }

        write!(f, "\n")
    }
}

impl From<Vec<Vec<Seat>>> for Area {
    fn from(input: Vec<Vec<Seat>>) -> Self {
        let mut res = Vec::new();

        for row in input {
            res.push(Row::from(row));
        }

        return Area(res);
    }
}

impl Into<Vec<Vec<Seat>>> for Area {
    fn into(self) -> Vec<Vec<Seat>> {
        let mut res = Vec::new();
        for row in self.0 {
            res.push(row.into());
        }

        return res;
    }
}

trait SeatingArea {
    fn get_seat(self: &Self, x: usize, y: usize) -> Option<&Seat>;
    fn get_area_flat(self: &Self, x: usize, y: usize) -> Vec<&Seat>;
    fn calc_change(self: &Self, x: usize, y: usize) -> Seat;
    fn update(self: &mut Self);
}

impl SeatingArea for Vec<Vec<Seat>> {
    fn get_seat(self: &Self, x: usize, y: usize) -> Option<&Seat> {
        self.get(y)?.get(x)
    }

    fn calc_change(self: &Self, x: usize, y: usize) -> Seat {
        let change = self
            .get_area_flat(x, y)
            .iter()
            .filter(|seat| ***seat == Seat::Occupied)
            .count();

        let seat = self.get_seat(x, y).unwrap();

        if *seat == Seat::Occupied && change >= 4 {
            return Seat::Empty;
        } else if *seat == Seat::Empty && change == 0 {
            return Seat::Occupied;
        } else {
            return seat.clone();
        }
    }

    fn get_area_flat(self: &Self, x: usize, y: usize) -> Vec<&Seat> {
        let seats = [
            (x.checked_sub(1), y.checked_add(1)),
            (Some(x), y.checked_add(1)),
            (x.checked_add(1), y.checked_add(1)),
            (x.checked_sub(1), Some(y)),
            (x.checked_add(1), Some(y)),
            (x.checked_sub(1), y.checked_sub(1)),
            (Some(x), y.checked_sub(1)),
            (x.checked_add(1), y.checked_sub(1)),
        ];

        seats
            .iter()
            .filter(|(x, y)| x.is_some() && y.is_some())
            .filter_map(|(x, y)| self.get_seat(x.unwrap(), y.unwrap()))
            .collect()
    }

    fn update(self: &mut Self) {
        todo!()
    }
}

#[aoc_generator(day11)]
fn input_generator(input: &str) -> Vec<Vec<Seat>> {
    input
        .lines()
        .map(|line| {
            line.as_bytes()
                .iter()
                .map(|state| Seat::from(state))
                .collect::<Vec<Seat>>()
        })
        .collect()
}

fn calc_change_whole(input: &Vec<Vec<Seat>>) -> Vec<Vec<Seat>> {
    let mut res = Vec::new();

    for y in 0..input.len() {
        let mut row = Vec::new();
        for x in 0..input[y].len() {
            row.push(input.calc_change(x, y));
        }

        res.push(row);
    }

    return res;
}

#[aoc(day11, part1)]
fn solver_part1(input: &Vec<Vec<Seat>>) -> usize {
    let mut curr = calc_change_whole(input);

    loop {
        let next = calc_change_whole(&curr);
        if *next == *curr {
            break;
        } else {
            curr = next;
        }
    }

    return curr
        .iter()
        .flatten()
        .filter(|seat| **seat == Seat::Occupied)
        .count();
}

#[cfg(test)]
mod test {
    use super::*;

    const INPUT: &str = "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
";

    fn get_input() -> Vec<Vec<Seat>> {
        input_generator(INPUT)
    }

    #[test]
    fn test_get_seat() {
        assert_eq!(Some(&Seat::Empty), get_input().get_seat(0, 0));
    }

    #[test]
    fn test_get_area_0_0() {
        let area = vec![&Seat::Empty, &Seat::Empty, &Seat::Floor];

        assert_eq!(area, get_input().get_area_flat(0, 0));
    }

    #[test]
    fn test_get_area_1_1() {
        let area = vec![
            &Seat::Empty,
            &Seat::Floor,
            &Seat::Empty,
            &Seat::Empty,
            &Seat::Empty,
            &Seat::Empty,
            &Seat::Floor,
            &Seat::Empty,
        ];

        assert_eq!(area, get_input().get_area_flat(1, 1));
    }

    #[test]
    fn test_calc_change() {
        let res = input_generator(
            "#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##
",
        );

        assert_eq!(Seat::Occupied, get_input().calc_change(1, 1));
        assert_eq!(Seat::Empty, res.calc_change(2, 1));
        assert_eq!(Seat::Occupied, res.calc_change(6, 0));
    }

    #[test]
    fn test_calc_change_whole_1() {
        let res = input_generator(
            "#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##
",
        );

        let result = calc_change_whole(&get_input());

        println!("{}", Area::from(result.clone()));

        assert_eq!(res, result);
    }

    #[test]
    fn test_calc_change_whole_2() {
        let pre = input_generator(
            "#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##
",
        );
        let post = input_generator(
            "#.LL.L#.##
#LLLLLL.L#
L.L.L..L..
#LLL.LL.L#
#.LL.LL.LL
#.LLLL#.##
..L.L.....
#LLLLLLLL#
#.LLLLLL.L
#.#LLLL.##
",
        );

        let result = calc_change_whole(&pre);

        println!("{}", Area::from(result.clone()));

        assert_eq!(post, result);
    }

    #[test]
    fn test_solver_part1() {
        assert_eq!(37, solver_part1(&get_input()))
    }
}
